# Copyright 2013-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=xtables-addons pn=xtables-addons suffix=tar.xz ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A set of extensions for the iptables/Xtables packet filter"
DESCRIPTION="
Xtables-addons provides some extra plugins for iptables. It obsoletes the old
patch-o-matic repository. Instead of patching the kernel source, extensions are
built as modules and thus allow extending kernels without recompilation.
"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        net-firewall/iptables[>=1.6.0]
        sys-kernel/linux-headers[>=4.15]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    CC=${CC}
    LD=${LD}
    CROSS_COMPILE=$(exhost --tool-prefix)
    HOSTCC=${CC}
    OBJDUMP=${OBJDUMP}
    libxtables_CFLAGS="-I/usr/$(exhost --target)/include"
    libxtables_LIBS=-lxtables
    --prefix=/usr
    --sbindir=/usr/$(exhost --target)/bin
    --libdir=/usr/$(exhost --target)/lib
    --libexecdir=/usr/$(exhost --target)/libexec
    --datarootdir=/usr/share
    --with-xtlibdir=/usr/$(exhost --target)/lib/xtables
    --without-kbuild
)

DEFAULT_SRC_COMPILE_PARAMS=(
    V=1
    CC=${CC}
    LD=${LD}
    OBJDUMP=${OBJDUMP}
)

DEFAULT_SRC_INSTALL_PARAMS=(
    CC=${CC}
    LD=${LD}
    CROSS_COMPILE=$(exhost --tool-prefix)
    HOSTCC=${CC}
    OBJDUMP=${OBJDUMP}
)

src_prepare() {
    # TODO: fix upstream
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)&:g" configure.ac

    autotools_src_prepare
}

src_test() {
    emake -j1 CC=${CC} LD=${LD} CROSS_COMPILE=$(exhost --tool-prefix) HOSTCC=${CC} OBJDUMP=${OBJDUMP} check
}

src_install() {
    default

    insinto /usr/src
    doins -r "${WORK}"/extensions
    edo mv "${IMAGE}"/usr/src/extensions "${IMAGE}"/usr/src/${PN}

    insinto /usr/src/${PN}
    doins "${WORK}"/mconfig
    [[ -f "${WORK}"/mconfig.* ]] && doins "${WORK}"/mconfig.*

    edo sed -i -e 's:${XA_ABSTOPSRCDIR}:/usr/src/'${PN}':' "${IMAGE}"/usr/src/${PN}/Kbuild
}

